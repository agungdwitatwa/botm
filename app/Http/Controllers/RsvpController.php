<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BotmData;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Str;

class RsvpController extends Controller
{

    public function landing()
    {
        $slot = BotmData::all('kegiatan');
        $data = [
            'slot' => $slot->countBy('kegiatan'),
        ];
        return view('welcome', $data);
    }

    public function insertData(Request $req)
    {
        $niks = BotmData::all('nik');

        foreach ($niks as $nik) {

            if ($nik->nik == $req->input('nik')) {
                return redirect('email-already-exists');
            }
        }

        $newData = [
            'id' => strtr(base64_encode(Str::uuid()), '+/=', '._-'),
            'nm_lkp' => $req->input('nm_lkp'),
            'nik' => $req->input('nik'),
            'email' => $req->input('email'),
            'daerah' => $req->input('daerah'),
            'nowa' => $req->input('nowa'),
            'instansi' => $req->input('instansi'),
            'jk' => $req->input('jk'),
            'kegiatan' => $req->input('kegiatan'),
        ];

        session()->flush();
        BotmData::create($newData);

        $sessionData = [
            'id' => $newData['id'],
            'noreg' => 'BOTM' . BotmData::select('noreg')->where('id', $newData['id'])->first()['noreg'] . $newData['kegiatan'] . '11',
            'nama' => $req->input('nm_lkp'),
            'daerah' => $req->input('daerah'),
            'tgl' => $newData['kegiatan']
        ];

        session($sessionData);

        return redirect('/success');
    }

    public function success()
    {
        return view('success');
    }

    public function download()
    {
        $img = \Image::make(public_path('img/editdikitasd.png'));

        $img->text(session()->get('nama'), 390, 125, function ($font) {
            $font->file(public_path('font/font.ttf'));
            $font->size(40);
            $font->color('#ffffff');
            $font->align('left');
            $font->valign('top');
        });

        $img->text(session()->get('daerah'), 390, 240, function ($font) {
            $font->file(public_path('font/font.ttf'));
            $font->size(15);
            $font->color('#ffffff');
            $font->align('left');
            $font->valign('top');
        });

        $img->text('' . session()->get('tgl') . " November 2021" . '', 597, 240, function ($font) {
            $font->file(public_path('font/font.ttf'));
            $font->size(15);
            $font->color('#ffffff');
            $font->align('left');
            $font->valign('top');
        });

        $img->text('No. Reg : ' . session()->get('noreg') . '', 90, 320, function ($font) {
            $font->file(public_path('font/font.ttf'));
            $font->size(15);
            $font->color('#ffffff');
            $font->align('left');
            $font->valign('top');
        });

        $qr = QrCode::format('png')->size(250)->color(14, 43, 92)->generate(url('validate/' . session()->get('id')));
        $img->insert('data:image/jpg;base64,' . base64_encode($qr), 'top-left', 47, 40);

        // return $img->response();

        $name = '' . session()->get('id') . '-' . session()->get('nama') . '.jpg';
        $image = $img->encode('jpg');

        session()->flush();

        $headers = [
            'Content-Type' => 'image/jpg',
            'Content-Disposition' => 'attachment; filename=' . $name,
        ];

        return response()->stream(function () use ($image) {
            echo $image;
        }, 200, $headers);
    }

    public function emailExists()
    {
        return view('emailexist');
    }

    public function validation($id)
    {
        $data = BotmData::find($id);
        if ($data) {
            echo $data->nm_lkp;
        } else {
            echo 'Tiket tidak valid';
        }
    }
}
