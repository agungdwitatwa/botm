<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBotmDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('botm_data', function (Blueprint $table) {
            $table->string('id');
            $table->increments('noreg');
            $table->string('nm_lkp');
            $table->string('nik')->unique();
            $table->string('email');
            $table->string('daerah');
            $table->string('nowa');
            $table->string('instansi');
            $table->string('jk');
            $table->string('kegiatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('botm_data');
    }
}
