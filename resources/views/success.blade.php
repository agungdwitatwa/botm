<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Montserrat:700' rel='stylesheet' type='text/css'>
    <style>
        @import url(//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.min.css);
        @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css);

        .button-37 {
            background-color: #13aa52;
            border: 1px solid #13aa52;
            border-radius: 4px;
            box-shadow: rgba(0, 0, 0, .1) 0 2px 4px 0;
            box-sizing: border-box;
            color: #fff;
            cursor: pointer;
            font-family: "Akzidenz Grotesk BQ Medium", -apple-system, BlinkMacSystemFont, sans-serif;
            font-size: 16px;
            font-weight: 400;
            outline: none;
            outline: 0;
            padding: 10px 25px;
            text-align: center;
            transform: translateY(0);
            transition: transform 150ms, box-shadow 150ms;
            user-select: none;
            -webkit-user-select: none;
            touch-action: manipulation;
        }

        .button-37:hover {
            box-shadow: rgba(0, 0, 0, .15) 0 3px 9px 0;
            transform: translateY(-2px);
        }

        @media (min-width: 768px) {
            .button-37 {
                padding: 10px 30px;
            }
        }

    </style>
    <link rel="stylesheet" href="https://2-22-4-dot-lead-pages.appspot.com/static/lp918/min/default_thank_you.css">
    <script src="https://2-22-4-dot-lead-pages.appspot.com/static/lp918/min/jquery-1.9.1.min.js"></script>
    <script src="https://2-22-4-dot-lead-pages.appspot.com/static/lp918/min/html5shiv.js"></script>
</head>

<body>
    <header class="site-header" id="header">
        <i class="fa fa-check main-content__checkmark" id="checkmark"></i>
        <h1 class="site-header__title" data-lead-id="site-header-title" style="color: #24b663">Sukses!!</h1>
        <p>{{ session()->get('nama') }}</p>
    </header>

    <div class="main-content">
        <p class="main-content__body" data-lead-id="main-content-body">Data Anda sudah berhasil tersimpan, silahkan klik
            tombol dibawah ini untuk mendownload tiket Anda</p>
        <p style="font-size: 13px; color:red"> * Note: Pastikan Anda langsung
            mendownload tiket, karena tiket hanya dapat di download satu kali</p>
        <br><br>
        @if (session()->get('nama'))
            <a href="{{ url('download-ticket') }}" class="button-37" role="button"
                style="color: #fff; text-decoration:none">Download Tiket</a>
        @else
            <p>Anda Sudah Mendowload Tiket</p>
        @endif
    </div>

    <footer class="site-footer" id="footer">
        <p class="site-footer__fineprint" id="fineprint">Copyright ©2021 | LPPNTB</p>
    </footer>
</body>

</html>
