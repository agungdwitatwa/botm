<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Registrasi Acara Beasiswa NTB On The Move">
    <meta name="developer" content="Agung Dwitawa">
    <title>Register | Beasiswa NTB On The Move</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{ url('img/favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{ url('img/apple-touch-icon-57x57-precomposed.png') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72"
        href="{{ url('img/apple-touch-icon-72x72-precomposed.png') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114"
        href="{{ url('img/apple-touch-icon-114x114-precomposed.png') }}">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400;700&display=swap"
        rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/style.css') }}" rel="stylesheet">
    <link href="{{ url('css/responsive.css') }}" rel="stylesheet">
    <link href="{{ url('css/menu.css') }}" rel="stylesheet">
    <link href="{{ url('css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/icon_fonts/css/all_icons_min.css') }}" rel="stylesheet">
    <link href="{{ url('css/skins/square/grey.css') }}" rel="stylesheet">
    <link href="{{ url('css/custom.css') }}" rel="stylesheet">
    <script src="{{ url('js/modernizr.js') }}"></script>

</head>

<body>

    <div id="preloader">
        <div data-loader="circle-side"></div>
    </div><!-- /Preload -->

    <div id="loader_form">
        <div data-loader="circle-side-2"></div>
    </div><!-- /loader_form -->

    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-3">
                    <h4>Beasiswa NTB On The Move</h4>
                </div>
                <div class="col-9">
                    <div id="social">
                        <ul>
                            <li><a href="#0"><i class="icon-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/bntbonthemove/"><i class="icon-instagram"></i></a>
                            </li>
                            <li><a href="https://www.youtube.com/channel/UCiwMtRu01J3BRjCgccNNXKg"><i
                                        class="icon-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </header>
    <!-- End Header -->

    <main>
        <div id="form_container">
            <div class="row">
                <div class="col-lg-5">
                    <div id="left_form">
                        <figure><img src="img/registration_bg.svg" alt=""></figure>
                        <h2>Registrasi</h2>
                        <p>Pastikan data yang Anda input pada form ini merupakan data yang Valid</p>
                        <a href="#0" id="more_info" data-bs-toggle="modal" data-bs-target="#more-info"><i
                                class="pe-7s-info"></i></a>
                    </div>
                </div>
                <div class="col-lg-7">

                    <div id="wizard_container">
                        <div id="top-wizard">
                            <div id="progressbar"></div>
                        </div>
                        <!-- /top-wizard -->
                        {{-- <form action="{{ url('submit-data') }}" name="example-1" id="wrapped" method="post"> --}}
                        <form action="{{ url('submit-data') }}" method="post">
                            @csrf
                            <input id="website" name="website" type="text" value="">
                            <!-- Leave for security protection, read docs for details -->
                            <div id="middle-wizard">
                                <div class="step">
                                    <h3 class="main_question"><strong>1/2</strong>Masukkan data Anda</h3>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="nm_lkp" class="form-control required"
                                                    placeholder="Nama Lengkap">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="nik" class="form-control required"
                                                    placeholder="NIK">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /row -->

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="email" name="email" class="form-control required"
                                                    placeholder="Email">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="styled-select">
                                                    <select class="required" name="daerah">
                                                        <option value="" selected>Asal Daerah</option>
                                                        <option value="Kota Mataram">Kota Mataram</option>
                                                        <option value="Lombok Barat">Lombok Barat</option>
                                                        <option value="Lombok Tengah">Lombok Tengah</option>
                                                        <option value="Lombok Timur">Lombok Timur</option>
                                                        <option value="Lombok Utara">Lombok Utara</option>
                                                        <option value="Sumbawa">Sumbawa</option>
                                                        <option value="Sumbawa Barat">Sumbawa Barat</option>
                                                        <option value="Bima">Bima</option>
                                                        <option value="Kota Bima">Kota Bima</option>
                                                        <option value="Dompu">Dompu</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /row -->

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="nowa" class="form-control"
                                                    placeholder="Nomor WA">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="instansi" class="form-control"
                                                    placeholder="instansi ">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group radio_input">
                                                <label><input type="radio" value="Laki - Laki" name="jk"
                                                        class="icheck">Laki - Laki</label>
                                                <label><input type="radio" value="Perempuan" name="jk"
                                                        class="icheck">Perempuan</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /row -->
                                </div>
                                <!-- /step-->
                                <div class="step submit">
                                    <h3 class="main_question"><strong>2/2</strong>Pilih tanggal kegiatan yang akan
                                        diikuti</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if (\Illuminate\Support\Arr::exists($slot, '28'))
                                                @if ($slot['28'] >= 85)
                                                    <div class="form-group radio_input">
                                                        <label><input type="radio" class="icheck required" disabled>
                                                            <span style="text-decoration: line-through">
                                                                28 November 2021
                                                            </span>
                                                            ( Slot sudah penuh )
                                                        </label>
                                                    </div>
                                                @else
                                                    <div class="form-group radio_input">
                                                        <label><input type="radio" value="28" name="kegiatan"
                                                                class="icheck required">
                                                            28 November 2021 ({{ 85 - $slot['28'] }} Slot tersisa)
                                                        </label>
                                                    </div>
                                                @endif
                                            @else
                                                <div class="form-group radio_input">
                                                    <label><input type="radio" value="28" name="kegiatan"
                                                            class="icheck required">
                                                        28 November 2021 (85 Slot tersisa)
                                                    </label>
                                                </div>
                                            @endif
                                            <div class="form-group radio_input">
                                                <label><input type="radio" class="icheck" disabled>
                                                    <span style="text-decoration: line-through">29
                                                        November 2021</span> (Hanya untuk Alumni Beasiswa NTB)
                                                </label>
                                            </div>
                                            <div class="form-group radio_input">
                                                @if (\Illuminate\Support\Arr::exists($slot, '30'))
                                                    @if ($slot['30'] >= 85)
                                                        <div class="form-group radio_input">
                                                            <label><input type="radio" class="icheck required" disabled>
                                                                <span style="text-decoration: line-through">
                                                                    30 November 2021
                                                                </span>
                                                                ( Slot sudah penuh )
                                                            </label>
                                                        </div>
                                                    @else
                                                        <div class="form-group radio_input">
                                                            <label><input type="radio" value="30" name="kegiatan"
                                                                    class="icheck required">
                                                                30 November 2021 ({{ 85 - $slot['30'] }} Slot
                                                                tersisa)
                                                            </label>
                                                        </div>
                                                    @endif
                                                @else
                                                    <div class="form-group radio_input">
                                                        <label><input type="radio" value="30" name="kegiatan"
                                                                class="icheck required">
                                                            30 November 2021 (85 Slot tersisa)
                                                        </label>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /row -->
                                </div>
                                <!-- /step-->
                            </div>
                            <!-- /middle-wizard -->
                            <div id="bottom-wizard">
                                <button type="button" name="backward" class="backward">Sebelumnya </button>
                                <button type="button" name="forward" class="forward">Selanjutnya</button>
                                <button type="submit" name="process" class="submit">Submit</button>
                            </div>
                            <!-- /bottom-wizard -->
                        </form>
                    </div>
                    <!-- /Wizard container -->
                </div>
            </div><!-- /Row -->
        </div><!-- /Form_container -->
    </main>

    <footer id="home" class="clearfix">
        <p>© 2021 Lembaga Pengembangan Pendidikan NTB</p>
        <ul>
            <li><a href="https://www.instagram.com/bntbonthemove/" target="_blank" class="animated_link">Instagram</a>
            </li>
            <li><a href="https://api.whatsapp.com/send?phone=+6285337369363" target="_blank"
                    class="animated_link">Kontak Kami</a></li>
        </ul>
    </footer>
    <!-- end footer-->

    <div class="cd-overlay-nav">
        <span></span>
    </div>

    <!-- Modal info -->
    <!-- /.modal -->

    <!-- SCRIPTS -->
    <!-- Jquery-->
    <script src="{{ url('js/jquery-3.6.0.min.js') }}"></script>
    <!-- Common script -->
    <script src="{{ url('js/common_scripts_min.js') }}"></script>
    <!-- Wizard script -->
    <script src="{{ url('js/registration_wizard_func.js') }}"></script>
    <!-- Menu script -->
    <script src="{{ url('js/velocity.min.js') }}"></script>
    <script src="{{ url('js/main.js') }}"></script>
    <!-- Theme script -->
    <script src="{{ url('js/functions.js') }}"></script>

</body>

</html>
